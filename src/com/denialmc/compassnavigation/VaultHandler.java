package com.denialmc.compassnavigation;

import net.milkbowl.vault.economy.Economy;

import org.bukkit.plugin.RegisteredServiceProvider;

public class VaultHandler {
	
	public Economy economy;
	
	public VaultHandler(CompassNavigation plugin) {
        RegisteredServiceProvider<Economy> provider = plugin.getServer().getServicesManager().getRegistration(Economy.class);
        if (provider != null) {
        	economy = provider.getProvider();
        }
	}
}