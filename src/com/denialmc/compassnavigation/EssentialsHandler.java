package com.denialmc.compassnavigation;

import org.bukkit.Location;

import com.earth2me.essentials.IEssentials;
import com.earth2me.essentials.api.IWarps;

public class EssentialsHandler {

	public IWarps warps;
	
	public EssentialsHandler(CompassNavigation plugin) {
		warps = ((IEssentials) plugin.getServer().getPluginManager().getPlugin("Essentials")).getWarps();
	}
	
	public Location getWarp(String warp) {
		try {
			warps.reloadConfig();
			return warps.getWarp(warp);
		} catch (Exception e) {
			return null;
		}
	}
}