package com.denialmc.compassnavigation;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionDefault;
import org.bukkit.plugin.java.JavaPlugin;

import com.denialmc.compassnavigation.AutoUpdater.UpdateResult;

public class CompassNavigation extends JavaPlugin implements Listener {
	
	public WorldGuardHandler worldGuardHandler;
	public ProtocolLibHandler protocolLibHandler;
	public VaultHandler vaultHandler;
	public AutoUpdater autoUpdater;
	public EssentialsHandler essentialsHandler;
	public LilypadHandler lilypadHandler;
	
	public HashMap<String, Inventory> inventories = new HashMap<String, Inventory>();
	public HashMap<String, String> sessions = new HashMap<String, String>();
	public HashMap<String, WarmupTimer> timers = new HashMap<String, WarmupTimer>();
	
	public static final char EMPTY_CHAR  = '\u0000';
	
	public void onEnable() {
		getConfig().options().copyDefaults(true);
		saveConfig();
        
        if (getServer().getPluginManager().isPluginEnabled("ProtocolLib")) {
        	protocolLibHandler = new ProtocolLibHandler(this);
        	getLogger().info("Hooked into ProtocolLib for enchantments and attribute removing!");
        }
        
        if (getServer().getPluginManager().isPluginEnabled("Essentials")) {
        	essentialsHandler = new EssentialsHandler(this);
        	getLogger().info("Hooked into Essentials for warps!");
        }
        
        if (getServer().getPluginManager().isPluginEnabled("WorldGuard")) {
        	worldGuardHandler = new WorldGuardHandler(this);
        	getLogger().info("Hooked into WorldGuard for region compass usage flag!");
        }
        
        if (getServer().getPluginManager().isPluginEnabled("Vault")) {
        	vaultHandler = new VaultHandler(this);
        	getLogger().info("Hooked into WorldGuard for economy!");
        }
        
        if (getServer().getPluginManager().isPluginEnabled("LilyPad-Connect")) {
        	lilypadHandler = new LilypadHandler(this);
        	getLogger().info("Hooked into LilyPad-Connect for LilyPad servers!");
        }
        
        if (!getDescription().getVersion().contains("SNAPSHOT") && getConfig().getBoolean("settings.autoUpdate")) {
        	autoUpdater = new AutoUpdater(this, getFile(), true);
        }
        
        loadInventories();
        
        getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
		getServer().getPluginManager().registerEvents(this, this);
	}
	
	public void loadInventories() {
		inventories.clear();
		
		getLogger().info("Loading inventories...");
		for (String name : getConfig().getConfigurationSection("settings").getKeys(false)) {
			if (getConfig().getString("settings." + name + ".title") != null) {
				try {
					Inventory inventory = getServer().createInventory(null, getConfig().getInt("settings." + name + ".rows") * 9, replacePlaceholders(ChatColor.translateAlternateColorCodes('&', getConfig().getString("settings." + name + ".title"))));
					
					for (int slot = 0; slot < (inventory.getSize() + 1); slot++) {
						ItemStack item = handleItem(name + ".", slot);
						if (item != null) {
							inventory.setItem(slot - 1, item);
						}
					}
					
					inventories.put(name, inventory);
					getLogger().info("Loaded inventory '" + name + "'.");
				} catch (Exception e) {
					getLogger().severe("Couldn't load inventory '" + name + "'. This is because of a wrongly set up config.");
					getLogger().severe("Technical exception: " + e.getMessage());
					getLogger().severe("More info on how to set it up correctly on http://goo.gl/sXdl3A");
				}
			}
		}
	}
	
	public boolean canUseCompass(Player player) {
		if (getConfig().getStringList("settings.disabledWorlds").contains(player.getWorld().getName()) && !player.hasPermission("compassnav.useanywhere")) {
			return false;
		} else if (worldGuardHandler != null && !player.hasPermission("compassnav.useanywhere")) {
			return worldGuardHandler.canUseCompassHere(player.getLocation());
		}
		return true;
	}
	
	public int getId(String id) {
    	return Integer.parseInt(id.split(":")[0]);
    }
    
    public short getDamage(String id) {
    	String[] split = id.split(":");
    	if (split.length >= 2) {
    		return Short.parseShort(split[1]);
    	}
    	return (short) 0;
    }
    
    public String replaceModifiers(Player player, String string) {
    	return string.replace("<name>", player.getName()).replace("<displayname>", player.getDisplayName()).replace("<x>", Integer.toString(player.getLocation().getBlockX())).replace("<y>", Integer.toString(player.getLocation().getBlockY())).replace("<z>", Integer.toString(player.getLocation().getBlockZ())).replace("<yaw>", Integer.toString((int) player.getLocation().getYaw())).replace("<pitch>", Integer.toString((int) player.getLocation().getPitch()));
    }
    
    public String replacePlaceholders(String string) {
    	return string.replace("<3", "\u2764").replace("[*]", "\u2605").replace("[**]", "\u2739").replace("[p]", "\u25CF").replace("[v]", "\u2714").replace("[+]", "\u25C6").replace("[++]", "\u2726").replace("[/]", "\u2588").replace("[cross]", "\u2720").replace("[arrow_right]", "\u27A1").replace("[arrow_down]", "\u2B07").replace("[arrow_left]", "\u2B05").replace("[arrow_up]", "\u2B06");
    }
    
    public List<String> translateList(List<String> list) {
    	List<String> translated = new ArrayList<String>();
    	for (String string : list) {
    		translated.add(ChatColor.translateAlternateColorCodes('&', replacePlaceholders(string)));
    	}
    	return translated;
    }
    
    public List<String> translateBungeeList(List<String> list, BungeeServer server) {
    	List<String> translated = new ArrayList<String>();
    	for (String string : list) {
    		translated.add(string.replace("<address>", server.getAddress()).replace("<port>", Integer.toString(server.getPort())).replace("<motd>", server.getMotd()).replace("<version>", server.getVersion()).replace("<players>", Integer.toString(server.getOnlinePlayers())).replace("<maxplayers>", Integer.toString(server.getMaxPlayers())));
    	}
    	return translated;
    }
    
    public Inventory cloneInventory(Player player, Inventory inventory) {
    	Inventory cloned = getServer().createInventory(null, inventory.getSize(), inventory.getTitle().replace("<player>", player.getName()));
    	cloned.setContents(inventory.getContents());
    	return cloned;
    }
    
    public Inventory replaceBungeeInfo(String name, Inventory inventory) {
    	for (String info : getConfig().getStringList("settings." + name + ".bungeeMappings")) {
    		try {
    			String[] splitAddress = info.split(":");
    			int slot = Integer.parseInt(splitAddress[0]) - 1;
    			String address = splitAddress[1];
    			int port = Integer.parseInt(splitAddress[2]);
    			BungeeServer server = connectToServer(address, port);
    			
    			if (server != null) {
    				ItemStack item = inventory.getItem(slot);
    				if (item.hasItemMeta()) {
    					ItemMeta meta = item.getItemMeta();
    					if (meta.hasDisplayName()) {
    						meta.setDisplayName(meta.getDisplayName().replace("<address>", server.getAddress()).replace("<port>", Integer.toString(server.getPort())).replace("<motd>", server.getMotd()).replace("<version>", server.getVersion()).replace("<players>", Integer.toString(server.getOnlinePlayers())).replace("<maxplayers>", Integer.toString(server.getMaxPlayers())));
    					}
    					if (meta.hasLore()) {
    						meta.setLore(translateBungeeList(meta.getLore(), server));
    					}
    					item.setItemMeta(meta);
    					inventory.setItem(slot, item);
    				}
    			}
    		} catch (Exception e) {
    			getLogger().warning("Couldn't parse '" + info + "', because: " + e.getMessage());
    		}
    	}
    	return inventory;
    }
    
    public ItemStack handleItem(String inventory, int slot) {
    	if (getConfig().contains("settings." + inventory + slot)) {
    		try {
    			String rawItem = getConfig().getString("settings." + inventory + slot + ".id", "2");
		    	String name = getConfig().getString("settings." + inventory + slot + ".name");
    			List<String> lore = translateList(getConfig().getStringList("settings." + inventory + slot + ".lore"));
		    	boolean enchanted = getConfig().getBoolean("settings." + inventory + slot + ".enchanted", false);
    			int amount = getConfig().getInt("settings." + inventory + slot + ".amount", 1);
    			ItemStack item = new ItemStack(getId(rawItem), amount, getDamage(rawItem));
    			ItemMeta meta = item.getItemMeta();
		    	
		    	if (name != null) {
		    		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', replacePlaceholders(ChatColor.RESET + name)));
		    	}
		    	
		    	if (!lore.isEmpty()) {
		    		meta.setLore(lore);
		    	}
		    	
		    	item.setItemMeta(meta);
				
				if (enchanted && protocolLibHandler != null) {
					item.addUnsafeEnchantment(Enchantment.WATER_WORKER, 4);
				}
				
				return item;
    		} catch (Exception e) {
    			getLogger().warning("Couldn't set item '" + slot + "' in inventory '" + inventory + "' This is because of a wrongly set up config.");
    			getLogger().warning("Technical exception: " + e.getMessage());
				getLogger().warning("More info on how to set it up correctly on http://goo.gl/sXdl3A");
    		}
    	}
    	return null;
    }
    
    public BungeeServer connectToServer(String address, int port) {
	    try {
	    	if (address.equals("this")) {
	    		return new BungeeServer(address, port, getServer().getVersion(), getServer().getMotd(), getServer().getOnlinePlayers().length, getServer().getMaxPlayers());
	    	} else {
	    		Socket socket = new Socket();
	    		
	    		socket.connect(new InetSocketAddress(address, port), 5000);
	    		
	    		DataOutputStream output = new DataOutputStream(socket.getOutputStream());
	    		DataInputStream input = new DataInputStream(socket.getInputStream());
	    		
	    		output.write(254);
	    		output.write(1);
	    		output.write(250);
	    		
	    		output.writeShort(11);
	    		output.writeChars("MC|PingHost");
	    		output.writeShort(7 + 2 * address.length());
	    		
	    		output.writeByte(73);
	    		
	    		output.writeShort(address.length());
	    		output.writeChars(address);
	    		output.writeInt(port);
	    		
	    		output.flush();
	    		
	    		if (input.read() != 255) {
	    			getLogger().warning("Server '" + address + ":" + port + "' returned an invalid response");
	    			socket.close();
	    			return null;
	    		}
	    		
	    		short bytes = input.readShort();
	    		StringBuilder builder = new StringBuilder();
	    		
	    		for (int i = 0; i < bytes; i++) {
	    			builder.append(input.readChar());
	    		}
	    		
	    		output.close();
	    		input.close();
	    		socket.close();
	    		
	   			String[] response = builder.toString().split(String.valueOf(EMPTY_CHAR));
	    		if (response.length < 4) {
	    			getLogger().warning("Server '" + address + ":" + port + "' returned an invalid response");
	   				return null;
	   			}
	   			
	   			return new BungeeServer(address, port, response[2], response[3], Integer.parseInt(response[4]), Integer.parseInt(response[5]));
	   		}
	   	} catch (Exception e) {
	   		e.printStackTrace();
	   		getLogger().warning("Couldn't create connection with server '" + address + ":" + port + "', because: " + e.getMessage());
	   		return null;
	   	}
    }
    
    public void openInventory(Player player, String name) {
    	if (player.hasPermission(new Permission("compassnav." + name, PermissionDefault.TRUE))) {
			if (inventories.containsKey(name)) {
				Inventory inventory = replaceBungeeInfo(name, cloneInventory(player, inventories.get(name)));
				sessions.put(player.getName(), name + ".");
				player.openInventory(inventory);
			} else {
				getLogger().severe("You do not have the inventory '" + name + "' set up, please set it up in the config.");
				getLogger().severe("More info on how to set it up on http://goo.gl/sXdl3A");
			}
    	}
	}
    
    public void checkUsage(Player player, String inventory, int slot) {
    	if (canUseCompass(player)) {
    		checkPlayers(player, inventory, slot);
    	} else {
    		player.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("general.cantUseMessage")));
    	}
    }
    
    public void checkPlayers(Player player, String inventory, int slot) {
    	if (timers.containsKey(player.getName())) {
    		timers.get(player.getName()).cancel();
    	}
    	
    	if (getConfig().getBoolean("settings.warmUp") && !player.hasPermission("compassnav.nowarmup") && !getConfig().contains("settings." + inventory + slot + ".inventory")) {
    		boolean delay = false;
    		
	    	for (Player p : player.getWorld().getPlayers()) {
	    		if (p.getName() != player.getName() && p.getLocation().distance(player.getLocation()) < getConfig().getInt("settings.warmUpDistance")) {
			    	delay = true;
			    	break;
	    		}
	    	}
	    	
	    	if (delay) {
	    		timers.put(player.getName(), new WarmupTimer(this, player, inventory, slot));
	    		timers.get(player.getName()).runTaskLater(this, 20L * getConfig().getInt("settings.warmUpTime"));
	    		player.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("general.warmUpMessage").replace("<time>", Integer.toString(getConfig().getInt("settings.warmUpTime")))));
	    	} else {
	    		checkMoney(player, inventory, slot);
	    	}
    	} else {
    		checkMoney(player, inventory, slot);
    	}
    }
    
    public void checkMoney(Player player, String inventory, int slot) {
    	if (vaultHandler != null && vaultHandler.economy != null && !player.hasPermission("compassnav.free")) {
	    	if (getConfig().contains("settings." + inventory + slot + ".price")) {
	    		if (vaultHandler.economy.has(player.getName(), getConfig().getDouble("settings." + inventory + slot + ".price"))) {
	    			vaultHandler.economy.withdrawPlayer(player.getName(), getConfig().getDouble("settings." + inventory + slot + ".price"));
	    			player.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("general.chargedMessage").replace("<price>", Double.toString(getConfig().getDouble("settings." + inventory + slot + ".price")))));
	    			executeCommands(player, inventory, slot);
	    		} else {
	    			player.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("general.notEnoughMessage")));
	    			player.closeInventory();
	    		}
	    	} else {
	    		executeCommands(player, inventory, slot);
	    	}
    	} else {
    		executeCommands(player, inventory, slot);
    	}
    }
    
    public void executeCommands(Player player, String inventory, int slot) {
    	for (String command : getConfig().getStringList("settings." + inventory + slot + ".commands")) {
    		if (command.startsWith("c:")) {
    			getServer().dispatchCommand(getServer().getConsoleSender(), replaceModifiers(player, command.substring(2)));
    		} else {
    			getServer().dispatchCommand(player, replaceModifiers(player, command));
    		}
    	}
    	
    	checkMessages(player, inventory, slot);
    }
    
    public void checkMessages(Player player, String inventory, int slot) {
    	for (String message : getConfig().getStringList("settings." + inventory + slot + ".messages")) {
    		player.sendMessage(replacePlaceholders(ChatColor.translateAlternateColorCodes('&', message.replace("<name>", player.getName()).replace("<displayname>", player.getDisplayName()))));
    	}
    	
    	checkInventory(player, inventory, slot);
    }
    
    public void checkInventory(Player player, String inventory, int slot) {
    	if (getConfig().contains("settings." + inventory + slot + ".inventory")) {
    		player.closeInventory();
    		openInventory(player, getConfig().getString("settings." + inventory + slot + ".inventory"));
    	} else {
    		checkBungee(player, inventory, slot);
    	}
    }
    
    public void checkBungee(Player player, String inventory, int slot) {
    	if (getConfig().contains("settings." + inventory + slot + ".bungee")) {
    		try {
				ByteArrayOutputStream byteOutput = new ByteArrayOutputStream();
				DataOutputStream output = new DataOutputStream(byteOutput);
				output.writeUTF("Connect");
				output.writeUTF(getConfig().getString("settings." + inventory + slot + ".bungee"));

				player.closeInventory();
				player.sendPluginMessage(this, "BungeeCord", byteOutput.toByteArray());
			} catch (Exception e) {
				checkLilypad(player, inventory, slot);
			}
    	} else {
    		checkLilypad(player, inventory, slot);
    	}
    }
    
    public void checkLilypad(Player player, String inventory, int slot) {
    	if (lilypadHandler != null) {
	    	if (getConfig().contains("settings." + inventory + slot + ".lilypad")) {
	    		player.closeInventory();
	    		if (!lilypadHandler.connect(player, getConfig().getString("settings." + inventory + slot + ".lilypad"))) {
	    			player.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("general.cantConnectMessage")));
	    		}
	    	} else {
	    		checkEssentials(player, inventory, slot);
	    	}
    	} else {
    		checkEssentials(player, inventory, slot);
    	}
    }
    
    public void checkEssentials(Player player, String inventory, int slot) {
    	if (essentialsHandler != null) {
    		if (getConfig().contains("settings." + inventory + slot + ".warp")) {
    			Location location = essentialsHandler.getWarp(getConfig().getString("settings." + inventory + slot + ".warp"));
    			if (location != null) {
    				player.teleport(location);
    				player.closeInventory();
    			} else {
    				checkLocation(player, inventory, slot);
    			}
    		} else {
    			checkLocation(player, inventory, slot);
    		}
    	} else {
    		checkLocation(player, inventory, slot);
    	}
    }
    
    public void checkLocation(Player player, String inventory, int slot) {
    	if (getConfig().contains("settings." + inventory + slot + ".world") && getConfig().contains("settings." + inventory + slot + ".x") && getConfig().contains("settings." + inventory + slot + ".y") && getConfig().contains("settings." + inventory + slot + ".z") && getConfig().contains("settings." + inventory + slot + ".yaw") && getConfig().contains("settings." + inventory + slot + ".pitch")) {
    		player.closeInventory();
    		player.teleport(new Location(getServer().getWorld(getConfig().getString("settings." + inventory + slot + ".world")), (float) getConfig().getDouble("settings." + inventory + slot + ".x"), (float) getConfig().getDouble("settings." + inventory + slot + ".y"), (float) getConfig().getDouble("settings." + inventory + slot + ".z"), (float) getConfig().getDouble("settings." + inventory + slot + ".yaw"), (float) getConfig().getDouble("settings." + inventory + slot + ".pitch")));
    	} else {
    		player.closeInventory();
    	}
    }
    
    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
    	Player player = event.getPlayer();
    	if (event.hasItem()) {
    		if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
	    		ItemStack item = event.getItem();
	    		if (getConfig().contains("settings.items." + item.getTypeId())) {
	    			openInventory(player, getConfig().getString("settings.items." + item.getTypeId()));
	    			event.setCancelled(true);
	    			return;
	    		} else if (getConfig().contains("settings.items." + item.getTypeId() + ":" + item.getDurability())) {
	    			openInventory(player, getConfig().getString("settings.items." + item.getTypeId() + ":" + item.getDurability()));
	    			event.setCancelled(true);
	    			return;
	    		}
    		}
    	}
    	
    	if (event.hasBlock()) {
    		if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
    			Block block = event.getClickedBlock();
    			if (block.getState() instanceof Sign) {
    				Sign sign = (Sign) block.getState();
    				if (sign.getLine(0).equals(ChatColor.translateAlternateColorCodes('&', getConfig().getString("settings.signTitle")))) {
    					event.setCancelled(true);
    					openInventory(player, sign.getLine(2));
    				}
    			}
    		}
    	}
    }
    
    @EventHandler
    public void onCommandPreprocess(PlayerCommandPreprocessEvent event) {
    	Player player = event.getPlayer();
    	String[] args = event.getMessage().substring(1).toLowerCase().split(" ");
    	if (args.length > 0) {
			String command = args[0];
			if (getConfig().contains("settings.commands." + command)) {
				event.setCancelled(true);
				String inventory = getConfig().getString("settings.commands." + command);
				if (args.length == 1) {
					openInventory(player, inventory);
				} else if (player.hasPermission("compassnav.opensomeone")) {
					Player target = getServer().getPlayer(args[1]);
					if (target != null) {
						openInventory(target, inventory);
						player.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("general.openedForMessage").replace("<player>", target.getName()).replace("<inventory>", inventory)));
					} else {
						player.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("general.notOnlineMessage").replace("<player>", args[1])));
					}
				} else {
					openInventory(player, inventory);
				}
			}
    	}
	}
    
    @EventHandler
	public void onInventoryClick(InventoryClickEvent event) {
		if (event.getWhoClicked() instanceof Player) {
			Player player = (Player) event.getWhoClicked();
			if (sessions.containsKey(player.getName())) {
				event.setCancelled(true);
				String inventory = sessions.get(player.getName());
				int slot = event.getRawSlot() + 1;
				if (getConfig().contains("settings." + inventory + slot)) {
					if (player.hasPermission(new Permission("compassnav." + inventory + slot, PermissionDefault.TRUE))) {
						if (getConfig().getBoolean("settings.sounds")) {
							player.playSound(player.getLocation(), Sound.valueOf(getConfig().getString("settings.teleportSound").toUpperCase()), 1.0F, 1.0F);
						}
						checkUsage(player, inventory, slot);
					} else if (getConfig().getBoolean("settings.sounds")) {
						player.playSound(player.getLocation(), Sound.valueOf(getConfig().getString("settings.noPermSound").toUpperCase()), 1.0F, 1.0F);
					}
				}
			}
		}
	}
    
    @EventHandler
	public void onPlayerMove(PlayerMoveEvent event) {
		Player player = event.getPlayer();
		if (timers.containsKey(player.getName())) {
			if (event.getTo().getX() != event.getFrom().getX() || event.getTo().getY() != event.getFrom().getY() || event.getTo().getZ() != event.getFrom().getZ()) {
				timers.get(player.getName()).cancel();
				timers.remove(player.getName());
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("general.teleportCancelMessage")));
			}
		}
	}
    
    @EventHandler
    public void onSignChange(SignChangeEvent event) {
    	Player player = event.getPlayer();
    	if (event.getLine(0).equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&', getConfig().getString("settings.signTitle"))) || event.getLine(0).equalsIgnoreCase(ChatColor.stripColor(ChatColor.translateAlternateColorCodes('&', getConfig().getString("settings.signTitle"))))) {
	    	if (player.hasPermission("compassnav.createsign")) {
	    		event.setLine(0, ChatColor.translateAlternateColorCodes('&', getConfig().getString("settings.signTitle")));
	    	} else {
	    		event.setLine(0, ChatColor.stripColor(ChatColor.translateAlternateColorCodes('&', getConfig().getString("settings.signTitle"))));
	    	}
    	}
    }
    
    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
    	Player player = event.getPlayer();
    	if (timers.containsKey(player.getName())) {
			timers.get(player.getName()).cancel();
			timers.remove(player.getName());
		}
    	sessions.remove(player.getName());
    }
    
    @EventHandler
    public void onInventoryClose(InventoryCloseEvent event) {
    	sessions.remove(event.getPlayer().getName());
    }
	
	public void sendHelpMessage(CommandSender sender) {
		for (String string : getConfig().getStringList("general.commandHelpMessage")) {
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&', string));
		}
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
  		if (cmd.getName().equalsIgnoreCase("cn") || cmd.getName().equalsIgnoreCase("compassnav")) {
  			if (args.length == 0) {
  				sendHelpMessage(sender);
  			} else if (args[0].equalsIgnoreCase("reload")) {
  				if (sender.hasPermission("compassnav.reload")) {
  					reloadConfig();
  					loadInventories();
  					sender.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("general.reloadMessage")));
  				} else {
  					sender.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("general.noPermMessage")));
  				}
  			} else if (args[0].equalsIgnoreCase("update")) {
  				if (sender.hasPermission("compassnav.update")) {
  					if (autoUpdater != null) {
  						autoUpdater.download();
  						if (autoUpdater.getResult() == UpdateResult.SUCCESS) {
  							sender.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("general.updateSuccessMessage")));
  						} else {
  							sender.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("general.updateFailedMessage")));
  						}
  					} else {
  						sender.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("general.cantUpdateMessage")));
  					}
  				} else {
  					sender.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("general.noPermMessage")));
  				}
  			} else if (args[0].equalsIgnoreCase("setup")) {
  				if (sender.hasPermission("compassnav.setup")) {
  					
  				} else {
  					sender.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("general.noPermMessage")));
  				}
  			} else {
  				sendHelpMessage(sender);
  			}
  		}
  		return true;
  	}
}