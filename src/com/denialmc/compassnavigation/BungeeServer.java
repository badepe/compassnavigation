package com.denialmc.compassnavigation;

public class BungeeServer {

	private String address;
	private int port;
	private String version;
	private String motd;
	private int onlinePlayers;
	private int maxPlayers;
	
	public BungeeServer(String address, int port, String version, String motd, int onlinePlayers, int maxPlayers) {
		this.address = address;
		this.port = port;
		this.version = version;
		this.motd = motd;
		this.onlinePlayers = onlinePlayers;
		this.maxPlayers = maxPlayers;
	}
	
	public String getAddress() {
		return address;
	}
	
	public int getPort() {
		return port;
	}
	
	public String getVersion() {
		return version;
	}
	
	public String getMotd() {
		return motd;
	}
	
	public int getOnlinePlayers() {
		return onlinePlayers;
	}
	
	public int getMaxPlayers() {
		return maxPlayers;
	}
}